#include "NumericAttribute.hpp"

template class NumericAttribute<int32>;
template class NumericAttribute<int64>;
template class NumericAttribute<float32>;
template class NumericAttribute<float64>;
template class NumericAttribute<Vector3>;
