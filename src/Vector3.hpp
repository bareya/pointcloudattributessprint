#ifndef VECTOR3_H
#define VECTOR3_H

#include "BasicTypes.hpp"

class Vector3
{
public:
    float32 x,y,z;
};

#endif // VECTOR3_H
