#ifndef INDEXEDACCESSOR_HPP
#define INDEXEDACCESSOR_HPP

#include "Attribute.hpp"
#include "GenericAccessorImpl.hpp"
#include "PointCloud.hpp"

#include <vector>
#include <algorithm>

///
/// Indexed attribute provides 'efficient' way of storing attributes
/// such as strings, we don't need to keep a n times copy of the string
/// we need one only.
///
template<typename T>
class IndexedAttribute : public Attribute
{
public:
    using value_type = T;

    IndexedAttribute(const std::string& t_name,
                     const PointCloud& t_pointcloud)
        : Attribute (t_name, t_pointcloud)
    {
    }

    Attribute* clone(PointCloud& new_pc) const override
    {
        return nullptr;
    }

    /// Returns a custom implementation to the attribute accessor.
    GenericAccessorHolder getAGA() const override
    {
        return GenericAccessorHolder{new GenericAccessorImpl()};
    }

    ///
    void setArraySize(arr_size s) override
    {
        m_indexes.resize(s);
    }

    void getItem(T& value, arr_index i) const
    {
        if(m_values.size() == 0 )
        {
            value = "";
            return;
        }

        auto value_index = m_indexes[i];
        value = m_values[value_index];
    }

    void setItem(const T& value, arr_index i)
    {
        // find if value exists in the array
        auto pos = std::find(std::begin(m_values), std::end(m_values), value);
        if(pos != std::end(m_values))
        {
            auto value_index = std::distance(std::begin(m_values), pos);
            m_indexes[i] = value_index;
        }
        else
        {
            auto value_index = m_values.size();
            m_values.emplace_back(value);
            m_indexes[i] = value_index;
        }
    }

private:
    std::vector<T> m_values;
    std::vector<arr_index> m_indexes;
};

using StringAttribute = IndexedAttribute<std::string>;

#endif // INDEXEDACCESSOR_HPP
