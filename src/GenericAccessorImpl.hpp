#ifndef GENERICACCESSORIMPL_HPP
#define GENERICACCESSORIMPL_HPP

#include "GenericAccessor.hpp"

class GenericAccessorImpl : public GenericAccessor
{
public:
    GenericAccessorImpl() = default;

    /// Since all of those methods are virtual methods performance drop is unavoidable.

    bool read(const Attribute* attrib, float32 &data, arr_index i) const override;
    bool write(Attribute* attrib, float32 data, arr_index i) const override;

    bool read(const Attribute* attrib, std::string& data, arr_index i) const override;
    bool write(Attribute* attrib, const std::string& data, arr_index i) const override;
};

#endif // GENERICACCESSORIMPL_HPP
