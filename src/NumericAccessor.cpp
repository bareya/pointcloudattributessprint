#include "NumericAccessor.hpp"
#include "TypeName.hpp"
#include "Vector3.hpp"

#include "NumericAttribute.hpp"

template<typename _Type, typename _Attr>
NumericAccessorR<_Type,_Attr>::NumericAccessorR(const Attribute *t_attribute)
{
    if(t_attribute->typeId() == typeid(_Attr))
    {
        m_attrib = dynamic_cast<const _Attr*>(t_attribute);
    }
    else
    {
        throw std::runtime_error(std::string{"Can't instanciate RW with given attribute: "}
                                 + t_attribute->getName() + " " + TypeName<_Attr>::value()  );
    }
}

template<typename _Type, typename _Attr>
const _Type NumericAccessorR<_Type, _Attr>::getValue(arr_index i) const
{
    return this->m_attrib->getArrayData()[i];
}

template<typename _Type, typename _Attr>
NumericAccessorW<_Type,_Attr>::NumericAccessorW(Attribute *t_attribute)
    : NumericAccessorR<_Type,_Attr>(t_attribute)
{
}

template<typename _Type, typename _Attr>
void NumericAccessorW<_Type,_Attr>::setValue(arr_index i, const _Type& value)
{
    const_cast<_Attr*>(this->m_attrib)->getArrayData()[i] = value;
}


template<typename _Type, typename _Attr>
NumericAccessorRW<_Type, _Attr>::NumericAccessorRW(Attribute *t_attrib)
    : NumericAccessorW<_Type, _Attr>(t_attrib)
{
}

///
///
///
#define INSTANTIATE_ACCESSOR(data_type, accessor) \
template class accessor ## R<data_type, NumericAttribute<data_type>>; \
template class accessor ## W<data_type, NumericAttribute<data_type>>; \
template class accessor ## RW<data_type, NumericAttribute<data_type>>;

///
/// instantiation
///
INSTANTIATE_ACCESSOR(int32, NumericAccessor)
INSTANTIATE_ACCESSOR(int64, NumericAccessor)
INSTANTIATE_ACCESSOR(float32, NumericAccessor)
INSTANTIATE_ACCESSOR(float64, NumericAccessor)
INSTANTIATE_ACCESSOR(Vector3, NumericAccessor)
