#ifndef ATTRIBUTEFACTORY_HPP
#define ATTRIBUTEFACTORY_HPP

#include "FwdTypes.hpp"

class PointCloud;

//enum class AttributeType
//{
//    INT32,
//    INT64,
//    FLOAT32,
//    FLOAT64,
//    VECTOR3,
//    STRING
//};

///
/// AttributeFactory spawns new attributes
///
class AttributeFactory
{
public:
    AttributeFactory(const AttributeFactory&) = delete;
    AttributeFactory(AttributeFactory&&) = delete;

    AttributeFactory& operator=(const AttributeFactory&) = delete;
    AttributeFactory& operator=(AttributeFactory&&) = delete;

    ~AttributeFactory();

    static AttributeFactory& instance();

    template<typename _Attr>
    void registerNew(const std::string& type)
    {
        registerNew(type, [](const std::string& name, const PointCloud& pc) -> AttributeHolder {
            return AttributeHolder{new _Attr(name, pc)};
        });
    }

    using AttributeCreate = std::function<AttributeHolder(const std::string&,
                                                          const PointCloud&)>;
    void registerNew(const std::string& type, AttributeCreate ctor);

    AttributeHolder create(const std::string& type,
                           const std::string& name,
                           const PointCloud& pc);

private:
    AttributeFactory();
};

#endif // ATTRIBUTEFACTORY_HPP
