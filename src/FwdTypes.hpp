#ifndef FWDTYPES_HPP
#define FWDTYPES_HPP

#include "BasicTypes.hpp"

#include <memory>

class Attribute;
using AttributeHolder = std::unique_ptr<Attribute>;

class GenericAccessor;
using GenericAccessorHolder = std::unique_ptr<const GenericAccessor>;

#endif // FWDTYPES_HPP
