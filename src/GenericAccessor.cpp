#include "GenericAccessor.hpp"

bool GenericAccessor::read(const Attribute *attrib, int32 &data, arr_index i) const
{
    return false;
}

bool GenericAccessor::write(Attribute *attrib, int32 data, arr_index i) const
{
    return false;
}

bool GenericAccessor::read(const Attribute *attrib, int64 &data, arr_index) const
{
    return false;
}

bool GenericAccessor::write(Attribute *attrib, int64 data, arr_index) const
{
    return false;
}

bool GenericAccessor::read(const Attribute*, float32&, arr_index ) const
{
    return false;
}

bool GenericAccessor::write(Attribute*, float32, arr_index) const
{
    return false;
}

bool GenericAccessor::read(const Attribute *, float64& , arr_index ) const
{
    return false;
}

bool GenericAccessor::write(Attribute*, float64, arr_index ) const
{
    return false;
}

bool GenericAccessor::read(const Attribute*, std::string&, arr_index) const
{
    return false;
}

bool GenericAccessor::write(Attribute*, const std::string&, arr_index) const
{
    return false;
}
