﻿#ifndef ATTRIBUTEDICTIONARY_HPP
#define ATTRIBUTEDICTIONARY_HPP

#include "FwdTypes.hpp"

#include <unordered_map>

class AttributeKey
{
public:
    AttributeKey(AttributeHolder& attrib);
    AttributeKey(const Attribute* attrib);

    const std::string& get() const;

    bool operator==(const AttributeKey& other) const
    {
        return get() == other.get();
    }

private:
    const Attribute* m_attrib;
};


namespace std
{
    template <>
    struct hash<AttributeKey>
    {
        size_t operator()(const AttributeKey& val) const
        {
            return hasher(val.get());
        }
        hash<string> hasher; // cached
    };
}

///
/// But why? We have std::map, the benefit comes when we want to change the implementation.
/// For isntacne we want to swap it to the list or set. This class will behave the same.
/// No one needs to know what is an underlying data structure under this dictionary.
///
/// Certain methods can throw.
///
class AttributeDictionary
{
public:
    AttributeDictionary();

    AttributeDictionary(const AttributeDictionary&) = delete;
    AttributeDictionary(AttributeDictionary&&) = delete;

    AttributeDictionary& operator=(const AttributeDictionary&) = delete;
    AttributeDictionary& operator=(AttributeDictionary&&) = delete;

    ~AttributeDictionary();

    void insert(AttributeHolder attrib);
    Attribute* find(const std::string& name) const;

    arr_size count() const
    {
        return m_attrs.size();
    }

    using AttributeStorage = std::unordered_map<AttributeKey, AttributeHolder>;

    template<typename T>
    class IteratorT
    {
    public:
        IteratorT(const AttributeDictionary& dict)
            : m_pos{dict.m_attrs.begin()}
            , m_end{dict.m_attrs.end()}
        {}

        /// Forward
        IteratorT& operator++()
        {
            ++m_pos;
            return *this;
        }

        bool operator!=(const IteratorT& other) const
        {
            return m_pos != other.m_end;
        }

        T& operator*()
        {
            return *m_pos->second.get();
        }

        T* operator->()
        {
            return m_pos->second.get();
        }

        bool finished()
        {
            return m_pos == m_end;
        }

    private:
        AttributeStorage::const_iterator m_pos;
        AttributeStorage::const_iterator m_end;
    };

    using ConstAttributeIterator = IteratorT<const Attribute>;
    using AttributeIterator = IteratorT<Attribute>;


    // only const attribute iterator is
    ConstAttributeIterator cbegin() const
    {
        return {*this};
    }
    ConstAttributeIterator cend() const
    {
        return {*this};
    }

    AttributeIterator begin()
    {
        return {*this};
    }

    AttributeIterator end()
    {
        return {*this};
    }


private:
    AttributeStorage m_attrs;
};

#endif // ATTRIBUTEDICTIONARY_HPP
