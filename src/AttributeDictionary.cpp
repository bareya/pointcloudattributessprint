#include "AttributeDictionary.hpp"
#include "Attribute.hpp"

#include <algorithm>


bool operator==(const std::pair<const AttributeKey, AttributeHolder>& other, const std::string& lala)
{
    return other.first.get() == lala;
}

AttributeDictionary::AttributeDictionary()
{
}

AttributeDictionary::~AttributeDictionary()
{
}

void AttributeDictionary::insert(AttributeHolder attrib)
{
    m_attrs.emplace(AttributeKey{attrib}, std::move(attrib));
}

Attribute *AttributeDictionary::find(const std::string &name) const
{
   auto found = std::find(std::begin(m_attrs), std::end(m_attrs), name);
   if(found == std::end(m_attrs))
   {
        throw std::runtime_error("Attribute not found");
   }

   return found->second.get();
}

AttributeKey::AttributeKey(AttributeHolder &attrib)
    : m_attrib{attrib.get()}
{
}

AttributeKey::AttributeKey(const Attribute *attrib)
    : m_attrib{attrib}
{
}

const std::string &AttributeKey::get() const
{
    return m_attrib->getName();
}
