#include <iostream>
#include <memory>
#include <vector>
#include <typeinfo>
#include <algorithm>
#include <exception>

using namespace std;

#include "PointCloud.hpp"
#include "Attribute.hpp"
#include "NumericAccessor.hpp"
#include "AttributeDictionary.hpp"
#include "Vector3.hpp"

template<typename T>
void heap_memory(unsigned long gbs)
{
    unsigned long size = (gbs*1024L*1024L*1024L)/sizeof(double);
    T* k = new T[size]{};
}

#include "AttributeDictionary.hpp"

int main()
{
    AttributeDictionary m_attribDict;
    for(auto it=m_attribDict.cbegin(); !it.finished(); ++it)
    {
        std::cout << it->getName() << std::endl;
    }

    PointCloud pc;
    pc.appendPoints(2000000);
    std::cout << pc.numPoints() << std::endl;

    pc.appendAttribute("Potato", "potato");

    // check for attributes
    Float32AccessorRW radiusRW;
    try
    {
        radiusRW = pc.findAttribute("radius");
    }
    catch (const std::exception& e)
    {
        std::cout << e.what() << std::endl;
        return 0;
    }

    pc.setPosition3(0, 1.1f, 2.2f, 3.3f);
    Vector3 gpos;
    pc.getPosition3(0, gpos);
    std::cout << gpos.x << " " << gpos.y <<  " " << gpos.z << std::endl;

    radiusRW.setValue(0, 123.4f);
    std::cout << radiusRW.getValue(0) << std::endl;

    return 0;
}
