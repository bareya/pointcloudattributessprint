#ifndef TYPENAME_HPP
#define TYPENAME_HPP

#include "BasicTypes.hpp"

class Vector3;
class PointCloud;

template<typename T>
struct TypeName
{
    static const char* value()
    {
        return nullptr;
    }
};

template<>
struct TypeName<int32>
{
    static const char* value()
    {
        return "Int32";
    }
};

template<>
struct TypeName<int64>
{
    static const char* value()
    {
        return "Int64";
    }
};

template<>
struct TypeName<float32>
{
    static const char* value()
    {
        return "Float32";
    }
};

template<>
struct TypeName<float64>
{
    static const char* value()
    {
        return "Float64";
    }
};

template<>
struct TypeName<Vector3>
{
    static const char* value()
    {
        return "Vector3";
    }
};

template<>
struct TypeName<std::string>
{
    static const char* value()
    {
        return "String";
    }
};

template<>
struct TypeName<PointCloud>
{
    static const char* value()
    {
        return "PointCloud";
    }
};

#endif // TYPENAME_HPP
