#include "GenericAccessorImpl.hpp"

#include "NumericAttribute.hpp"
#include "IndexedAttribute.hpp"

bool GenericAccessorImpl::read(const Attribute *attrib, float32 &data, arr_index i) const
{
    if(attrib->typeId() == typeid(Float32Attribute))
    {
        auto f_attrib = dynamic_cast<const Float32Attribute*>(attrib);
        data = f_attrib->getArrayData()[i];
        return true;
    }
    return false;
}

bool GenericAccessorImpl::write(Attribute *attrib, float32 data, arr_index i) const
{
    if(attrib->typeId() == typeid(Float32Attribute))
    {
        auto f_attrib = dynamic_cast<Float32Attribute*>(attrib);
        f_attrib->getArrayData()[i] = data;
        return true;
    }
    return false;
}

bool GenericAccessorImpl::read(const Attribute *attrib, std::string &data, arr_index i) const
{
    if(attrib->typeId() == typeid(StringAttribute))
    {
        auto s_attrib = dynamic_cast<const StringAttribute*>(attrib);
        s_attrib->getItem(data, i);
        return true;
    }
    return false;
}

bool GenericAccessorImpl::write(Attribute *attrib, const std::string &data, arr_index i) const
{
    if(attrib->typeId() == typeid(StringAttribute))
    {
        auto s_attrib = dynamic_cast<StringAttribute*>(attrib);
        s_attrib->setItem(data, i);
        return true;
    }
    return false;
}
