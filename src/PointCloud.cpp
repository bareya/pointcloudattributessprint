#include "PointCloud.hpp"

#include "Attribute.hpp"
#include "AttributeFactory.hpp"
#include "Vector3.hpp"

#include <algorithm>
#include <exception>

PointCloud::PointCloud()
{
    /// appendAttribute uses 'this'. It's perfectly fine to
    /// use this in the constructor, first object allocaiton
    /// happens, then constructor is executed. Therefore it's
    /// okay to use this in the constructor.
    m_position = appendAttribute("Vector3", "P");
    m_radius = appendAttribute("Float32", "radius");
    m_id = appendAttribute("Int64", "id");
}

arr_size PointCloud::numPoints() const
{
    return m_numPoints;
}

arr_size PointCloud::appendPoint()
{
    m_numPoints++;
    for(Attribute& attrib : m_attribDict)
    {
        attrib.setArraySize(m_numPoints);
    }
    return m_numPoints-1;
}

arr_size PointCloud::appendPoints(arr_size s)
{
    m_numPoints+=s;

    for(Attribute& attrib : m_attribDict)
    {
        attrib.setArraySize(m_numPoints);
    }
    return m_numPoints-s;
}

Attribute *PointCloud::appendAttribute(const std::string& type,
                                       const std::string &name)
{
    /// force to throw if attribute exists
    auto attrib = findAttribute(name);
    if(attrib)
    {
        throw std::runtime_error(std::string("Attribute ") + name + " already exists");
    }

    auto new_attribute = AttributeFactory::instance().create(type, name, *this);
    new_attribute->setArraySize(m_numPoints);
    Attribute* r_attrib = new_attribute.get();

    m_attribDict.insert(std::move(new_attribute));
    return r_attrib;
}

Attribute *PointCloud::findAttribute(const std::string &name)
{
    try {
        return m_attribDict.find(name);
    }
    catch (const std::exception&)
    {
        return nullptr;
    }
}

arr_size PointCloud::numAttributes() const
{
    return m_attribDict.count();
}

void PointCloud::setPosition3(arr_index i, const Vector3 &vec)
{
    m_position.setValue(i, vec);
}

void PointCloud::setPosition3(arr_index i, float32 x, float32 y, float32 z)
{
    Vector3 pos{x,y,z};
    setPosition3(i, pos);
}

void PointCloud::getPosition3(arr_index i, Vector3 &vec) const
{
    vec = m_position.getValue(i);
}

bool PointCloud::merge(PointCloud&, const PointCloud&) const
{
    // TODO resolve merging
    return false;
}

const AttributeDictionary &PointCloud::getAttributeDictionary() const
{
    return m_attribDict;
}
