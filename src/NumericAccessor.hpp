#ifndef ATTRIBUTERW_H
#define ATTRIBUTERW_H

///
/// Those are specialized accessors, this header file can be
/// public, but once specialized templates are instanciated in cpp
/// file, then user can't add
///
/// An istance of each tample with it's type can be found in cpp file
///
/// There is one thing here what's bothering me. I have been hiding Attribute
/// implementations from the clients, they are compiler mentioned only and can be found
/// in attributeimpl.hpp and attributeimpl.cpp. Those files are private and can't be
/// distrbuted, therefore I don't know what is underlying type under the Attribute,
/// I have to declare it explicitly.
///
/// During the Accessor construction Attribute type has to be propely binded with
/// underlying type. For instance you create attribute Int64Attributre and then
/// you are tying to access its values through Int32AccessorRW, that will throw
/// during the construction. You are binding something that has data type
/// mismatch. Since Attribute is an abstract base and using dynamic dispatching
/// then attribute type is not know until runtime, and we can't check at compile time.
///
/// Constructor is not explicit because we want to have an implicit cast
/// from attribute to the reader.
/// Int32AccessorR reader = attribute; // will work just fine.
///

#include "FwdTypes.hpp"
#include "TypeName.hpp"

class Vector3;

template<typename _Type, typename _Attr>
class NumericAccessorR
{
public:
    NumericAccessorR(){}
    NumericAccessorR(const Attribute* t_attribute);

    const _Type getValue(arr_index i) const;

protected:
    /// cached reader
    const _Attr* m_attrib{nullptr};
};

template<typename _Type, typename _Attr>
class NumericAccessorW : public NumericAccessorR<_Type, _Attr>
{
public:
    NumericAccessorW(){}
    NumericAccessorW(Attribute* t_attribute);

    /// a reference here, right, is this okay for basic types?
    /// If a reference is just a pointer, an address, is it going to take
    /// more space than a small type? sizeof(char*) != sizeof(char)
    /// Shall I use move semantics here and pass it by value?
    void setValue(arr_index i, const _Type& value);

protected:
    /// Hiding reading implementation
    using NumericAccessorR<_Type,_Attr>::getValue;
};

template<typename _Type, typename _Attr>
class NumericAccessorRW : public NumericAccessorW<_Type, _Attr>
{
public:
    NumericAccessorRW(){}
    NumericAccessorRW(Attribute* t_attrib);

    /// exposing back get value to make it public.
    using NumericAccessorW<_Type,_Attr>::getValue;
};

///
/// Forward declarations of the common templates.
/// Those are defined somewhere in the bianry.
/// User doesn't have to know about them.
///
template<typename T>
class NumericAttribute;

///
/// extern informs that template instantiation is in external translation unit.
/// I am not sure if it's possible to instanciate, or mark extern aliased templates.
/// That would be really good and would skip a lot of typing.
/// I could do some dirty macro for this.
///
#define DECLARE_NUMERIC_ACCESSOR(data_type, accessor, name) \
using name ## R = accessor ## R<data_type, NumericAttribute<data_type>>; \
using name ## W = accessor ## W<data_type, NumericAttribute<data_type>>; \
using name ## RW = accessor ## RW<data_type, NumericAttribute<data_type>>; \
extern template class accessor ## R<data_type, NumericAttribute<data_type>>; \
extern template class accessor ## W<data_type, NumericAttribute<data_type>>; \
extern template class accessor ## RW<data_type, NumericAttribute<data_type>>;

///
/// Reader/Writer aliases that are instanciated inside of the cpp file.
///
DECLARE_NUMERIC_ACCESSOR(int32, NumericAccessor, Int32Accessor)
DECLARE_NUMERIC_ACCESSOR(int64, NumericAccessor, Int64Accessor)
DECLARE_NUMERIC_ACCESSOR(float32, NumericAccessor, Float32Accessor)
DECLARE_NUMERIC_ACCESSOR(float64, NumericAccessor, Float64Accessor)
DECLARE_NUMERIC_ACCESSOR(Vector3, NumericAccessor, Vector3Accessor)

#endif // ATTRIBUTERW_H
