#ifndef NUMERICATTRIBUTE_HPP
#define NUMERICATTRIBUTE_HPP

#include "Attribute.hpp"
#include "Vector3.hpp"
#include "GenericAccessorImpl.hpp"

#include <vector>


///
/// Any numeric type can be put into this template class.
/// At the end of the file you can find all aliases for numeric types.
///
template<typename T>
class NumericAttribute : public Attribute
{
public:
    using value_type = T;

    NumericAttribute(const std::string& t_name,
                     const PointCloud& t_pointcloud)
        : Attribute(t_name, t_pointcloud)
    {
    }

    Attribute* clone(PointCloud& new_pc) const override
    {
        return nullptr;
    }

    GenericAccessorHolder getAGA() const override
    {
        return GenericAccessorHolder{new GenericAccessorImpl()};
    }

    void setArraySize(arr_size s) override
    {
        m_values.resize(s);
    }

    const std::vector<T>& getArrayData() const
    {
        return m_values;
    }

    std::vector<T>& getArrayData()
    {
        return m_values;
    }

private:
    std::vector<T> m_values;
};

extern template class NumericAttribute<int32>;
extern template class NumericAttribute<int64>;
extern template class NumericAttribute<float32>;
extern template class NumericAttribute<float64>;
extern template class NumericAttribute<Vector3>;

///
/// There are always tradeoffs, those templates are going to increase size of the binary
/// but saves me a lot of typing.
///
using Int32Attribute = NumericAttribute<int32>;
using Int64Attribute = NumericAttribute<int64>;
using Float32Attribute = NumericAttribute<float32>;
using Float64Attribute = NumericAttribute<float64>;
using Vector3Attribute = NumericAttribute<Vector3>;

#endif // NUMERICATTRIBUTE_HPP
