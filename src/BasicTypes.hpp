#ifndef TYPENAME_H
#define TYPENAME_H

#include <iosfwd>

using int32 = int;
using int64 = long;
using float32 = float;
using float64 = double;

using arr_index = unsigned long;
using arr_size = unsigned long;

#endif // TYPENAME_H
