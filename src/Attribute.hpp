#ifndef ATTRIBUTE_H
#define ATTRIBUTE_H

#include "FwdTypes.hpp"

#include <typeinfo>
#include <map>

class GenericAccessor;
class PointCloud;

///
/// An attribute interface to store some information on a point cloud
///
class Attribute
{
public:
    /// An attribute must be attached to a point cloud
    Attribute(const std::string& t_name,
              const PointCloud& t_pointcloud);

    Attribute(const Attribute&) = delete;
    Attribute(Attribute&&) = delete;

    Attribute& operator=(const Attribute&) = delete;
    Attribute& operator=(Attribute&&) = delete;

    /// Must be virtual since we are going add custom implementations
    virtual ~Attribute();

    /// Clone attribute into new PointCloud
    virtual Attribute* clone(PointCloud& new_pc) const = 0;

    /// This is a generic accessor that provides access through
    /// virtual calls. This is an example only, to show performance
    /// penelties comming from virtual calls.
    /// There is a trade-off, do you want to have generic class with
    /// virtual calls, or a very specialized class that does a specific job.
    /// Calling read/write can be very slow, depending on size of the app.
    virtual GenericAccessorHolder getAGA() const = 0;

    /// naivly sets size of underlying array.
    virtual void setArraySize(arr_index size) = 0;

    /// RTTI for the attributes
    const std::type_info& typeId() const;

    /// Some common getters and setters for attribute details
    const std::string& getName() const;
    void setName(const std::string& name);

    const PointCloud& getPointCloud() const;

private:
    std::string m_name; /// Every attribute must have a name.

    const PointCloud& m_pointcloud; /// A reference to an object that created the attribute.
};

inline const std::type_info &Attribute::typeId() const
{
    return typeid(*this);
}

inline const std::string &Attribute::getName() const
{
    return m_name;
}

inline const PointCloud& Attribute::getPointCloud() const
{
    return m_pointcloud;
}

#endif // ATTRIBUTE_H
