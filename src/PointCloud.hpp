#ifndef POINTCLOUD_H
#define POINTCLOUD_H

#include "BasicTypes.hpp"
#include "AttributeDictionary.hpp"
#include "NumericAccessor.hpp"

#include <list>

///
/// A very basic structure that holds the point cloud data
/// Internally it holds the attributes. When points are appeneded
/// attribute's data is tracked automatically.
///
class PointCloud
{
public:
    PointCloud();
    ~PointCloud() = default;

    PointCloud(const PointCloud&) = delete;
    PointCloud(PointCloud&&) = delete;

    PointCloud& operator=(const PointCloud&) = delete;
    PointCloud& operator=(PointCloud&&) = delete;

    /// Append a point, all attributes will be defaulted.
    arr_size numPoints() const;
    arr_size appendPoint();
    arr_size appendPoints(arr_size s);

    const AttributeDictionary& getAttributeDictionary() const;

    /// Append an attribute, throw if exists
    Attribute* appendAttribute(const std::string& type,
                               const std::string& name);

    //Attribute* appendAttribute(AttributeHolder source);

    /// Find an attrbiute, method throws if it doesn't exists.
    Attribute* findAttribute(const std::string& name);

    ///
    arr_size numAttributes() const;

    /// Those methods access cached accessors,
    /// Generic accessor that derives from AttributeAccessor is much slower
    /// than specialized accessor.
    void setPosition3(arr_index i, const Vector3& vec);
    void setPosition3(arr_index i, float32 x, float32 y, float32 z);

    void getPosition3(arr_index i, Vector3& vec) const;

    /// Merge two point clouds into another
    bool merge(PointCloud& out, const PointCloud& other) const;

private:
    /// Total number of points in the point cloud.
    arr_size m_numPoints{};

    Vector3AccessorRW m_position; /// pre-cached vector data accessor
    Float32AccessorRW m_radius; /// pre-cached a float data accessor
    Int64AccessorRW m_id; /// pre-cached long data accessor

    AttributeDictionary m_attribDict;

};



#endif // POINTCLOUD_H
