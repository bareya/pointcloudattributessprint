#include "AttributeFactory.hpp"

#include "TypeName.hpp"

#include "NumericAttribute.hpp"
#include "IndexedAttribute.hpp"

static std::map<std::string, AttributeFactory::AttributeCreate > the_attributeFactoryMap;

bool operator==(const std::pair<std::string, AttributeFactory::AttributeCreate>& type_name, const std::string& other_type_name)
{
    return type_name.first == other_type_name;
}

AttributeFactory& AttributeFactory::instance()
{
    static AttributeFactory f;
    return f;
}

AttributeHolder AttributeFactory::create(const std::string& type,
                                         const std::string& name,
                                         const PointCloud& pc)
{
    auto found = std::find(std::begin(the_attributeFactoryMap), std::end(the_attributeFactoryMap), type);
    if(found == std::end(the_attributeFactoryMap))
    {
        throw std::runtime_error("Can not create an attribute with given type: " + type + ". Type does not exist.");
    }

    return the_attributeFactoryMap.at(type)(name, pc);
}

void AttributeFactory::registerNew(const std::string &type, AttributeFactory::AttributeCreate ctor)
{
    if(the_attributeFactoryMap.find(type) == the_attributeFactoryMap.end())
    {
        the_attributeFactoryMap.emplace(type, ctor);
    }
    else
    {
        throw std::runtime_error("Map contains given attribute. Overlaps are not allowed");
    }
}

template<typename T>
void registerNewAttribute(AttributeFactory* f)
{
    /// very nice... a small bug I faced, this type can be called
    /// with following syntax too:
    /// using value_type = typename T::value_type();
    /// but now it's not a type, but a declaration of a function.
    ///
    using value_type = typename T::value_type;
    const char* value_type_name = TypeName<value_type>::value();
    f->registerNew<T>(value_type_name);
}

AttributeFactory::AttributeFactory()
{
    registerNewAttribute<Int32Attribute>(this);
    registerNewAttribute<Int64Attribute>(this);
    registerNewAttribute<Float32Attribute>(this);
    registerNewAttribute<Float64Attribute>(this);
    registerNewAttribute<Vector3Attribute>(this);
    registerNewAttribute<StringAttribute>(this);
}

AttributeFactory::~AttributeFactory()
{
}

