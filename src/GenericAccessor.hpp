#ifndef ACCESSOR_HPP
#define ACCESSOR_HPP

#include "BasicTypes.hpp"

/// AGA - Attribute Generic Accessor
/// ASA - Attribute Specialized Accessor

class Attribute;

/// This is an example of all in one, very easy to be extended
/// We need to override give virtual methods, but there is a cost
/// to that. We can't neglect it.
class GenericAccessor
{
public:
    virtual ~GenericAccessor(){};

    /// Write/read a single int32 value
    virtual bool read(const Attribute* attrib, int& data, arr_index i) const;
    virtual bool write(Attribute* attrib, int data, arr_index i) const;

    /// Write/read a single int64 value
    virtual bool read(const Attribute* attrib, long& data, arr_index i) const;
    virtual bool write(Attribute* attrib, long data, arr_index i) const;

    /// Write/read a single float32 value
    virtual bool read(const Attribute* attrib, float& data, arr_index i) const;
    virtual bool write(Attribute* attrib, float data, arr_index i) const;

    /// Write/read a single float64 value
    virtual bool read(const Attribute* attrib, double& data, arr_index i) const;
    virtual bool write(Attribute* attrib, double data, arr_index i) const;

    /// Write/read a single string value
    virtual bool read(const Attribute* attrib, std::string& data, arr_index i) const;
    virtual bool write(Attribute* attrib, const std::string& data, arr_index i) const;
};

#endif // ACCESSOR_HPP
