cmake_minimum_required(VERSION 3.8)

project(PointCloud CXX)

add_definitions(-std=c++14 -Wall)
add_library(PointCloud SHARED)

target_sources(PointCloud
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/src/FwdTypes.hpp
        ${CMAKE_CURRENT_LIST_DIR}/src/BasicTypes.hpp
        ${CMAKE_CURRENT_LIST_DIR}/src/TypeName.hpp
        ${CMAKE_CURRENT_LIST_DIR}/src/Attribute.hpp
        ${CMAKE_CURRENT_LIST_DIR}/src/AttributeFactory.hpp
        ${CMAKE_CURRENT_LIST_DIR}/src/AttributeDictionary.hpp
        ${CMAKE_CURRENT_LIST_DIR}/src/Vector3.hpp
        ${CMAKE_CURRENT_LIST_DIR}/src/GenericAccessor.hpp
        ${CMAKE_CURRENT_LIST_DIR}/src/NumericAccessor.hpp
        ${CMAKE_CURRENT_LIST_DIR}/src/PointCloud.hpp
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/src/Attribute.cpp
        ${CMAKE_CURRENT_LIST_DIR}/src/NumericAttribute.cpp
        ${CMAKE_CURRENT_LIST_DIR}/src/NumericAttribute.hpp
        ${CMAKE_CURRENT_LIST_DIR}/src/IndexedAttribute.hpp
        ${CMAKE_CURRENT_LIST_DIR}/src/IndexedAttribute.cpp
        ${CMAKE_CURRENT_LIST_DIR}/src/AttributeFactory.cpp
        ${CMAKE_CURRENT_LIST_DIR}/src/GenericAccessor.cpp
        ${CMAKE_CURRENT_LIST_DIR}/src/GenericAccessorImpl.hpp
        ${CMAKE_CURRENT_LIST_DIR}/src/GenericAccessorImpl.cpp
        ${CMAKE_CURRENT_LIST_DIR}/src/NumericAccessor.cpp
        ${CMAKE_CURRENT_LIST_DIR}/src/AttributeDictionary.cpp
        ${CMAKE_CURRENT_LIST_DIR}/src/Vector3.cpp
        ${CMAKE_CURRENT_LIST_DIR}/src/PointCloud.cpp
    )

add_executable(testing
        "src/main.cpp"
    )

target_link_libraries(testing
    PUBLIC
        PointCloud
    )
